var showInfo = false

function seeInfoAction () {
    showInfo = !showInfo
    const div = document.querySelector(".see-info"); // <div class="info"></div>
    if (showInfo) {
        div.innerHTML = "<div><b>Por:</b> Magdyeli Fermín</div><div><b>Carrera:</b> Ingenieria en Informática</div><div><b>Fecha de creación:</b> 03/04/2023</div>"
    } else {
        div.innerHTML = ""; 
    }
}